package com.connect4;

public class Validator implements IValidator {
	@Override
	public int checkWin(Board board) {
		Cell[][] cells = board.getAllCells();
		for (int i = 0; i < board.getNumberOfRows() - 3; i++) {
			for (int j = 0; j < board.getNumberOfColumns(); j++) {
				Color colorOfCurrentToken = cells[i][j].getColor();
				if (colorOfCurrentToken == Color.WHITE)
					continue;
				if (colorOfCurrentToken == cells[i + 3][j].getColor()) {
					if (colorOfCurrentToken == cells[i + 2][j].getColor()) {
						if (colorOfCurrentToken == cells[i + 1][j].getColor()) {
							return checkWinningPlayer(colorOfCurrentToken);
						}
					}
				}
			}
		}
		for (int i = 0; i < board.getNumberOfRows(); i++) {
			for (int j = 0; j < board.getNumberOfColumns() - 3; j++) {
				Color colorOfCurrentToken = cells[i][j].getColor();
				if (colorOfCurrentToken == Color.WHITE)
					continue;
				if (colorOfCurrentToken == cells[i][j + 3].getColor()) {
					if (colorOfCurrentToken == cells[i][j + 2].getColor()) {
						if (colorOfCurrentToken == cells[i][j + 1].getColor()) {
							return checkWinningPlayer(colorOfCurrentToken);
						}
					}
				}
			}
		}
		for (int i = 0; i < board.getNumberOfRows() - 3; i++) {
			for (int j = 0; j < board.getNumberOfColumns() - 3; j++) {
				Color colorOfCurrentToken = cells[i][j].getColor();
				if (colorOfCurrentToken == Color.WHITE)
					continue;
				if (colorOfCurrentToken == cells[i + 3][j + 3].getColor()) {
					if (colorOfCurrentToken == cells[i + 2][j + 2].getColor()) {
						if (colorOfCurrentToken == cells[i + 1][j + 1].getColor()) {
							return checkWinningPlayer(colorOfCurrentToken);
						}
					}
				}
			}
		}

		for (int i = 3; i < board.getNumberOfRows(); i++) {
			for (int j = 0; j < board.getNumberOfColumns() - 3; j++) {
				Color colorOfCurrentToken = cells[i][j].getColor();
				if (colorOfCurrentToken == Color.WHITE)
					continue;

				if (colorOfCurrentToken == cells[i - 3][j + 3].getColor()) {
					if (colorOfCurrentToken == cells[i - 2][j + 2].getColor()) {
						if (colorOfCurrentToken == cells[i - 1][j + 1].getColor()) {
							return checkWinningPlayer(colorOfCurrentToken);
						}
					}
				}
			}
		}
		return 0;
	}

	private int checkWinningPlayer(Color colorOfCurrentToken) {
		if (colorOfCurrentToken == Color.RED) {
			return 1;
		} else {
			return 2;
		}
	}
}