package com.connect4;

public class Cell {
	private Color color;

	public Cell() {
		color = Color.WHITE;
	}

	public Color getColor() {
		return color;
	}

	public void putToken(Color color) {
		this.color = color;
	}

	public boolean isFree() {
		return color == Color.WHITE;
	}
}