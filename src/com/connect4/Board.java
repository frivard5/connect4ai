package com.connect4;

import java.util.ArrayList;
import java.util.List;

public class Board {
	private int numberOfColumns;
	private int numberOfRows;
	private Cell[][] cells;

	public Board() {
		numberOfColumns = 7;
		numberOfRows = 6;
		cells = createGameCells();
	}

	public int getNumberOfColumns() {
		return numberOfColumns;
	}

	public int getNumberOfRows() {
		return numberOfRows;
	}

	public Cell[][] getAllCells() {
		return cells;
	}

	public void play(int column, Color player) {
		int row = findDeepestEmptyCell(column);
		if (row == -1) {
			throw new IllegalArgumentException("Column full. Choose another one");
		}
		else {
			cells[row][column].putToken(player);
		}
	}

	public Cell getCell(int row, int column) {
		return cells[row][column];
	}

	public List<Integer> getValidColumns() {
		List<Integer> validColumns = new ArrayList<>();
		for (int j = 0; j < numberOfColumns; j++) {
			if (findDeepestEmptyCell(j) != -1) {
				validColumns.add(j);
			}
		}
		return validColumns;
	}

	private Cell[][] createGameCells() {
		Cell[][] cells = new Cell[6][7];
		for (int i = 0; i < numberOfRows; i++) {
			for (int j = 0; j < numberOfColumns; j++) {
				cells[i][j] = new Cell();
			}
		}
		return cells;
	}

	private int findDeepestEmptyCell(int column) {
		for (int i = 0; i < numberOfRows; i++) {
			if (cells[i][column].isFree()) {
				return i;
			}
		}
		return -1;
	}
}