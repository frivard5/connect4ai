package com.connect4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ui implements IUI {
	private Cell[][] cells;
	private String output;

	@Override
	public int getPlayerOneColumnChoice() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Joueur 1: choisissez une colonne [0-6]");
		int chosenCol = 0;
		try {
			chosenCol = Integer.parseInt(br.readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return chosenCol;
	}

	//⚫⚪
	@Override
	public void upDateVisual(Board board) {
		cells = board.getAllCells();
		output = "";
		for (int i = board.getNumberOfRows()-1; i > -1; i--) {
			for (int j = 0; j < board.getNumberOfColumns(); j++) {
				if (cells[i][j].getColor().equals(Color.RED)) {
					output = output + "|R|";
				} else if (cells[i][j].getColor().equals(Color.YELLOW)) {
					output = output + "|Y|";
				} else {
					output = output + "| |";
				}
			}
			output = output+"\n";
		}
		System.out.print(output);
		System.out.println("|                   |");
		System.out.println("|                   |");
		System.out.println();System.out.println();
	}

	@Override
	public int getPlayerTwoColumnChoice() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Joueur 2: choisissez une colonne [0-6]");
		int chosenCol = 0;
		try {
			chosenCol = Integer.parseInt(br.readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return chosenCol;
	}

	@Override
	public void draw() {
		System.out.println("Partie nulle, veuillez recommencer.");
	}

	@Override
	public void playerOneWin() {
		System.out.println("																											");
		System.out.println("██████╗ ██████╗  █████╗ ██╗   ██╗ ██████╗          ██╗ ██████╗ ██╗   ██╗███████╗██╗   ██╗██████╗      ██╗	");
		System.out.println("██╔══██╗██╔══██╗██╔══██╗██║   ██║██╔═══██╗         ██║██╔═══██╗██║   ██║██╔════╝██║   ██║██╔══██╗    ███║	");
		System.out.println("██████╔╝██████╔╝███████║██║   ██║██║   ██║         ██║██║   ██║██║   ██║█████╗  ██║   ██║██████╔╝    ╚██║	");
		System.out.println("██╔══██╗██╔══██╗██╔══██║╚██╗ ██╔╝██║   ██║    ██   ██║██║   ██║██║   ██║██╔══╝  ██║   ██║██╔══██╗     ██║	");
		System.out.println("██████╔╝██║  ██║██║  ██║ ╚████╔╝ ╚██████╔╝    ╚█████╔╝╚██████╔╝╚██████╔╝███████╗╚██████╔╝██║  ██║     ██║	");
		System.out.println("╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝  ╚═══╝   ╚═════╝      ╚════╝  ╚═════╝  ╚═════╝ ╚══════╝ ╚═════╝ ╚═╝  ╚═╝     ╚═╝	");
		System.out.println("                                                                                                         	");
	}

	@Override
	public void playerTwoWin() {
		System.out.println("																														");
		System.out.println("██╗   ██╗ ██████╗ ██╗   ██╗███████╗     █████╗ ██╗   ██╗███████╗███████╗    ██████╗ ███████╗██████╗ ██████╗ ██╗   ██╗	");
		System.out.println("██║   ██║██╔═══██╗██║   ██║██╔════╝    ██╔══██╗██║   ██║██╔════╝╚══███╔╝    ██╔══██╗██╔════╝██╔══██╗██╔══██╗██║   ██║	");
		System.out.println("██║   ██║██║   ██║██║   ██║███████╗    ███████║██║   ██║█████╗    ███╔╝     ██████╔╝█████╗  ██████╔╝██║  ██║██║   ██║	");
		System.out.println("╚██╗ ██╔╝██║   ██║██║   ██║╚════██║    ██╔══██║╚██╗ ██╔╝██╔══╝   ███╔╝      ██╔═══╝ ██╔══╝  ██╔══██╗██║  ██║██║   ██║	");
		System.out.println(" ╚████╔╝ ╚██████╔╝╚██████╔╝███████║    ██║  ██║ ╚████╔╝ ███████╗███████╗    ██║     ███████╗██║  ██║██████╔╝╚██████╔╝	");
		System.out.println("  ╚═══╝   ╚═════╝  ╚═════╝ ╚══════╝    ╚═╝  ╚═╝  ╚═══╝  ╚══════╝╚══════╝    ╚═╝     ╚══════╝╚═╝  ╚═╝╚═════╝  ╚═════╝ 	");
		System.out.println("                                                                                                                     	");

	}
}
