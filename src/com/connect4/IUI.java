package com.connect4;

public interface IUI {
	int getPlayerOneColumnChoice();

	void upDateVisual(Board board);

	int getPlayerTwoColumnChoice();

	void draw();

	void playerOneWin();

	void playerTwoWin();
}