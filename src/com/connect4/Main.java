package com.connect4;

public class Main {

	public static void main(String[] args) {

		Validator validator = new Validator();
		Ui ui = new Ui();
		Game game = new Game(ui,validator);
		System.out.println("    ____  __  ________________ ___    _   ______________   __ __  	");
		System.out.println("   / __ \\/ / / /  _/ ___/ ___//   |  / | / / ____/ ____/  / // /	");
		System.out.println("  / /_/ / / / // / \\__ \\\\__ \\/ /| | /  |/ / /   / __/    / // /_	");
		System.out.println(" / ____/ /_/ // / ___/ /__/ / ___ |/ /|  / /___/ /___   /__  __/	");
		System.out.println("/_/    \\____/___//____/____/_/  |_/_/ |_/\\____/_____/     /_/   	");
		System.out.println("																	");
		System.out.println();
		game.play();
	}
}