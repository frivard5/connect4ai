package com.connect4;

import com.rits.cloning.Cloner;

import java.util.List;

public class Game {

	private static final int MINIMAX_DEPTH = 5;
	private final Board board;
	private IUI ui;
	private IValidator validator;
	private boolean isPlayerOneTurn;

	public Game(IUI ui, IValidator validator) {
		board = new Board();
		this.ui = ui;
		isPlayerOneTurn = true;
		this.validator = validator;
	}

	public Board getBoard() {
		return board;
	}

	public void play() {
		ui.upDateVisual(getBoard());
		for (int i = 0; i < 42; i++) {

			if (isPlayerOneTurn) {
				playPlayerOneTurn();
				System.out.println("player1 score : " + score(board, Color.RED));
			} else {
				playPlayerTwoTurn();
				System.out.println("ai score : " + score(board, Color.YELLOW));
			}
			if (checkWinner() == 1) {
				ui.playerOneWin();
				return;
			} else if (checkWinner() == 2) {
				ui.playerTwoWin();
				return;
			}
		}
		ui.draw();
	}

	private int checkWinner() {
		return validator.checkWin(board);
	}

	private void playPlayerOneTurn() {
		int column = ui.getPlayerOneColumnChoice();
		board.play(column, Color.RED);
		ui.upDateVisual(getBoard());
		isPlayerOneTurn = false;
	}

	private void playPlayerTwoTurn() {
		int column = minimax(board, MINIMAX_DEPTH, true).getSecond();
		board.play(column, Color.YELLOW);
		ui.upDateVisual(getBoard());
		isPlayerOneTurn = true;
	}

	private IntegerPair minimax(Board board, int depth, boolean maxPlayer) {
		int value = 0;
		int score = 0;
		int bestColumn = board.getValidColumns().get(0);
		if (validator.checkWin(board) == 1) {
			return new IntegerPair(Integer.MIN_VALUE, bestColumn);
		}
		if (validator.checkWin(board) == 2) {
			return new IntegerPair(Integer.MAX_VALUE, bestColumn);
		}
		if (depth == 0) {
			value = score(board, Color.YELLOW);
			return new IntegerPair(value, bestColumn);
		}
		if (maxPlayer) {
            /*Tour de MAX*/
            value = Integer.MIN_VALUE;
            List<Integer> validColumns = board.getValidColumns();
            Cloner cloner = new Cloner();
            for (int column : validColumns) {
            	Board boardDeepCopy = cloner.deepClone(board);
				boardDeepCopy.play(column, Color.YELLOW);
            	score = minimax(boardDeepCopy, depth - 1, false).getFirst();
            	if (score > value) {
            		value = score;
            		bestColumn = column;
				}
			}
			return new IntegerPair(value, bestColumn);
		}
		else {
            /*Tour de MIN*/
			value = Integer.MAX_VALUE;
			List<Integer> validColumns = board.getValidColumns();
			Cloner cloner = new Cloner();
			for (int column : validColumns) {
				Board boardDeepCopy = cloner.deepClone(board);
				boardDeepCopy.play(column, Color.RED);
				score = minimax(boardDeepCopy, depth - 1, true).getFirst();
				if (score < value) {
					value = score;
					bestColumn = column;
				}
			}
			return new IntegerPair(value, bestColumn);
		}
	}

	private int score(Board board, Color playerColor) {
		int score = 0;
		Cell[][] cells = board.getAllCells();
		/*Score vertical*/
		for (int i = 0; i < board.getNumberOfRows(); i++) {
			for (int j = 0; j < board.getNumberOfColumns(); j++) {
				if (cells[i][j].getColor() != playerColor) {
					continue;
				}
				if (i < board.getNumberOfRows() - 3 &&
						playerColor == cells[i + 1][j].getColor() &&
						playerColor == cells[i + 2][j].getColor() &&
						playerColor == cells[i + 3][j].getColor()) {
					score += 100000; /* nombre beaucoup plus élevé car victoire */
					continue;
				}
				if (i < board.getNumberOfRows() - 2 &&
						playerColor == cells[i + 1][j].getColor() &&
						playerColor == cells[i + 2][j].getColor()) {
					score += 100;
					continue;
				}
				if (i < board.getNumberOfRows() - 1 &&
						playerColor == cells[i + 1][j].getColor()) {
					score += 10;
				}
			}
		}
		/*Score horizontal*/
		for (int i = 0; i < board.getNumberOfRows(); i++) {
			for (int j = 0; j < board.getNumberOfColumns(); j++) {
				if (cells[i][j].getColor() != playerColor) {
					continue;
				}
				if (j < board.getNumberOfColumns() - 3 &&
						playerColor == cells[i][j + 1].getColor() &&
						playerColor == cells[i][j + 2].getColor() &&
						playerColor == cells[i][j + 3].getColor()) {
					score += 100000; /* nombre beaucoup plus élevé car victoire */
					j += 3;
					continue;
				}
				if (j < board.getNumberOfColumns() - 2 &&
						playerColor == cells[i][j + 1].getColor() &&
						playerColor == cells[i][j + 2].getColor()) {
					score += 100;
					j += 2;
					continue;
				}
				if (j < board.getNumberOfColumns() - 1 &&
						playerColor == cells[i][j + 1].getColor()) {
					score += 10;
					j += 1;
				}
			}
		}
		/*Score diagonal positive*/
		for (int i = 0; i < board.getNumberOfRows(); i++) {
			for (int j = 0; j < board.getNumberOfColumns(); j++) {
				if (cells[i][j].getColor() != playerColor) {
					continue;
				}
				if (i < board.getNumberOfRows() - 3 &&
						j < board.getNumberOfColumns() - 3 &&
						playerColor == cells[i + 1][j + 1].getColor() &&
						playerColor == cells[i + 2][j + 2].getColor() &&
						playerColor == cells[i + 3][j + 3].getColor()) {
					score += 100000; /* nombre beaucoup plus élevé car victoire */
					continue;
				}
				if (i < board.getNumberOfRows() - 2 &&
						j < board.getNumberOfColumns() - 2 &&
						playerColor == cells[i + 1][j + 1].getColor() &&
						playerColor == cells[i + 2][j + 2].getColor()) {
					score += 100;
					continue;
				}
				if (i < board.getNumberOfRows() - 1 &&
						j < board.getNumberOfColumns() - 1 &&
						playerColor == cells[i + 1][j + 1].getColor()) {
					score += 10;
				}
			}
		}
		/*Score diagonal negative*/
		for (int i = 1; i < board.getNumberOfRows(); i++) {
			for (int j = 0; j < board.getNumberOfColumns(); j++) {
				if (cells[i][j].getColor() != playerColor) {
					continue;
				}
				if (i >= 3 &&
						j < board.getNumberOfColumns() - 3 &&
						playerColor == cells[i - 1][j + 1].getColor() &&
						playerColor == cells[i - 2][j + 2].getColor() &&
						playerColor == cells[i - 3][j + 3].getColor()) {
					score += 100000; /* nombre beaucoup plus élevé car victoire */
					continue;
				}
				if (i >= 2 &&
						j < board.getNumberOfColumns() - 2 &&
						playerColor == cells[i - 1][j + 1].getColor() &&
						playerColor == cells[i - 2][j + 2].getColor()) {
					score += 100;
					continue;
				}
				if (j < board.getNumberOfColumns() - 1 &&
						playerColor == cells[i - 1][j + 1].getColor()) {
					score += 10;
				}
			}
		}
		return score;
	}
}