package test;

import com.connect4.Board;
import com.connect4.Game;
import com.connect4.IUI;
import com.connect4.IValidator;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class GameTest {

	private Game game;
	private MockUi mockUi;
	private MockValidator mockValidator;

	@Before
	public void setUp() {
		mockUi = new MockUi();
		mockValidator = new MockValidator();
		game = new Game(mockUi, mockValidator);
	}

	@Test
	public void canConstructGame() {
		//Then
		assertNotNull(game);
	}

	@Test
	public void gameBoardIsNotNull() {
		//Then
		assertNotNull(game.getBoard());
	}

	@Test
	public void play_normalCondition_playersDontPlayTwiceInARow() {
		//When
		game.play();

		//Then
		//No assert here, as the Mock will handle the assertions..
	}

	@Test
	public void play_normalCondition_gameShouldStopAfter42Tokens() {
		//When
		game.play();

		//Then
		assertEquals(41, mockUi.turnNumber);
		assertTrue(mockUi.drawCalled);
	}

	@Test
	public void play_playerOneWins_shouldCallUiToShowPlayerOneWon() {
		//Given
		mockValidator.setValidationAnswer(1);
		//When
		game.play();
		//Then
		assertTrue(mockUi.playerOneWinCalled);
	}

	@Test
	public void play_playerTwoWins_shouldCallUiToShowPlayerTwoWon() {
		//Given
		mockValidator.setValidationAnswer(2);
		//When
		game.play();
		//Then
		assertTrue(mockUi.playerTwoWinCalled);
	}

	private class MockValidator implements IValidator {

		private int validationAnswer;

		public MockValidator() {
		}

		public void setValidationAnswer(int validationAnswer) {
			this.validationAnswer = validationAnswer;
		}

		@Override
		public int checkWin(Board board) {
			return validationAnswer;
		}
	}

	private class MockUi implements IUI {

		private int playerOnePlayed;
		private int playerTwoPlayed;
		private int turnNumber;
		private boolean drawCalled;
		private boolean playerOneWinCalled;
		private boolean playerTwoWinCalled;

		public MockUi() {
			playerOnePlayed = 0;
			playerTwoPlayed = 0;
			turnNumber = -1;
		}

		@Override
		public int getPlayerOneColumnChoice() {
			playerOnePlayed++;
			playerTwoPlayed--;
			turnNumber++;
			return getValidColumnNumber();
		}

		@Override
		public void upDateVisual(Board board) {
			assertFalse((playerOnePlayed == 1 && playerTwoPlayed == 0) || (playerOnePlayed == 0 && playerTwoPlayed == 1));
		}

		@Override
		public int getPlayerTwoColumnChoice() {
			playerTwoPlayed++;
			playerOnePlayed--;
			turnNumber++;
			return getValidColumnNumber();
		}

		@Override
		public void draw() {
			drawCalled = true;
		}

		@Override
		public void playerOneWin() {
			playerOneWinCalled = true;
		}

		@Override
		public void playerTwoWin() {
			playerTwoWinCalled = true;
		}

		private int getValidColumnNumber() {
			if (turnNumber < 6)
				return 0;
			if (turnNumber < 12)
				return 1;
			if (turnNumber < 18)
				return 2;
			if (turnNumber < 24)
				return 3;
			if (turnNumber < 30)
				return 4;
			if (turnNumber < 36)
				return 5;
			return 6;
		}
	}
}