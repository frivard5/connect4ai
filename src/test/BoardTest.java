package test;

import com.connect4.Board;
import com.connect4.Cell;
import com.connect4.Color;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

public class BoardTest {

	private Board board;

	@Before
	public void SetUp() {
		board = new Board();
	}

	@Test
	public void gameBoardIsSevenColumnAndSixRows() {
		//Then
		assertEquals(6, board.getNumberOfRows());
		assertEquals(7, board.getNumberOfColumns());
	}

	@Test
	public void boardShouldHaveFortyTwoCells() {
		//Given
		Cell[][] allCells = board.getAllCells();
		//Then
		assertEquals(42, allCells.length * allCells[0].length);
	}

	@Test
	public void gameBoardInitializeCellsCorrectly() {
		//Then
		for (Cell[] row : board.getAllCells()) {
			for (Cell cell : row) {
				assertNotNull(cell);
			}
		}
	}

	@Test
	public void play_emptyRow_tokenFallsAtTheBottom() {
		//When
		board.play(0, Color.RED);

		//Then
		assertEquals(Color.RED, board.getCell(0, 0).getColor());
	}

	@Test
	public void play_oneTokenInRow_tokenFallsOnTopOfTheOther() {
		//Given
		board.play(0, Color.RED);

		//When
		board.play(0, Color.YELLOW);

		//Then
		assertEquals(Color.YELLOW, board.getCell(1, 0).getColor());
	}

	@Test(expected = IllegalArgumentException.class)
	public void play_fullRow_ShouldThrowException() {
		//Given
		fillAColumn(0);

		//When
		board.play(0, Color.RED);
	}

	public void fillAColumn(int column) {
		board.play(column, Color.RED);
		board.play(column, Color.YELLOW);
		board.play(column, Color.RED);
		board.play(column, Color.YELLOW);
		board.play(column, Color.RED);
		board.play(column, Color.YELLOW);
	}
}