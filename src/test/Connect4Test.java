package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
		BoardTest.class,
		CellTest.class,
		GameTest.class,
		ValidatorTest.class
})
public class Connect4Test {
	public Connect4Test() {
	}
}