package test;

import com.connect4.Cell;
import com.connect4.Color;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class CellTest {

	private Cell cell;

	@Before
	public void setUp() {
		cell = new Cell();
	}

	@Test
	public void cellStartsEmpty() {
		//Then
		assertEquals(Color.WHITE, cell.getColor());
	}

	@Test
	public void cellCanBePlayed_yellowToken_cellShouldBeYellow() {
		//When
		cell.putToken(Color.YELLOW);
		//Then
		assertEquals(Color.YELLOW, cell.getColor());
	}

	@Test
	public void cellCanBePlayed_redToken_cellShouldBeRed() {
		//When
		cell.putToken(Color.RED);
		//Then
		assertEquals(Color.RED, cell.getColor());
	}

	@Test
	public void isCellEmpty_whiteCell_shouldBeEmpty() {
		//Then
		assertTrue(cell.isFree());
	}

	@Test
	public void isCellEmpty_redCell_shouldNotBeEmpty() {
		//When
		cell.putToken(Color.RED);
		//Then
		assertFalse(cell.isFree());
	}

	@Test
	public void isCellEmpty_yellowCell_shouldNotBeEmpty() {
		//When
		cell.putToken(Color.YELLOW);
		//Then
		assertFalse(cell.isFree());
	}
}