package test;

import com.connect4.Board;
import com.connect4.Cell;
import com.connect4.Color;
import com.connect4.Validator;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class ValidatorTest {

	private static final String ANSI_RED = "\u001B[31m";
	private static final String ANSI_YELLOW = "\u001B[33m";
	private static final String ANSI_RESET = "\u001B[0m";
	private Board board;
	private Validator validator;
	private Cell[][] cells;

	@Before
	public void setUp() throws Exception {
		board = new Board();
		validator = new Validator();
		cells = board.getAllCells();
	}

	@Test
	public void checkWin_verticalRedWin_shouldReturnOne() {
		//Given
		Color player = Color.RED;
		createVerticalWin(player);

		//When
		int answer = validator.checkWin(board);

		//Then
		assertEquals(1, answer);
	}

	@Test
	public void checkWin_verticalRedWinLastColumn_shouldReturnOne() {
		//Given
		Color player = Color.RED;
		createLastColumnVerticalWin(player);

		//When
		int answer = validator.checkWin(board);

		//Then
		assertEquals(1, answer);
	}

	@Test
	public void checkWin_horizontalRedWin_shouldReturnOne() {
		//Given
		Color player = Color.RED;
		createHorizontalWin(player);

		//When
		int answer = validator.checkWin(board);

		//Then
		assertEquals(1, answer);
	}

	@Test
	public void checkWin_forwardDiagonalRedWin_shouldReturnOne() {
		//Given
		Color player = Color.RED;
		createForwardDiagonalWin(player);

		//When
		int answer = validator.checkWin(board);

		//Then
		assertEquals(1, answer);
	}

	@Test
	public void checkWin_backwardDiagonalRedWin_shouldReturnOne() {
		//Given
		Color player = Color.RED;
		createBackwardDiagonalWin(player);

		//When
		int answer = validator.checkWin(board);

		//Then
		assertEquals(1, answer);
	}

	@Test
	public void checkWin_noWin_shouldReturnZero() {
		//When
		int answer = validator.checkWin(board);

		//Then
		assertEquals(0, answer);
	}

	@Test
	public void checkWin_verticalYellowWin_shouldReturnTwo() {
		//Given
		Color player = Color.YELLOW;
		createVerticalWin(player);

		//When
		int answer = validator.checkWin(board);

		//Then
		assertEquals(2, answer);
	}

	@Test
	public void checkWin_horizontalYellowWin_shouldReturnTwo() {
		//Given
		Color player = Color.YELLOW;
		createHorizontalWin(player);

		//When
		int answer = validator.checkWin(board);

		//Then
		assertEquals(2, answer);
	}

	@Test
	public void checkWin_forwardDiagonalYellowWin_shouldReturnTwo() {
		//Given
		Color player = Color.YELLOW;
		createForwardDiagonalWin(player);

		//When
		int answer = validator.checkWin(board);

		//Then
		assertEquals(2, answer);
	}

	@Test
	public void checkWin_backwardDiagonalYellowWin_shouldReturnTwo() {
		//Given
		Color player = Color.YELLOW;
		createBackwardDiagonalWin(player);

		//When
		int answer = validator.checkWin(board);

		//Then
		assertEquals(2, answer);
	}

	private void createVerticalWin(Color player) {
		cells[0][0].putToken(player);
		cells[1][0].putToken(player);
		cells[2][0].putToken(player);
		cells[3][0].putToken(player);
	}

	private void createHorizontalWin(Color player) {
		cells[0][0].putToken(player);
		cells[0][1].putToken(player);
		cells[0][2].putToken(player);
		cells[0][3].putToken(player);
	}

	private void createForwardDiagonalWin(Color player) {
		cells[0][0].putToken(player);
		cells[1][1].putToken(player);
		cells[2][2].putToken(player);
		cells[3][3].putToken(player);
	}

	private void createBackwardDiagonalWin(Color player) {
		cells[3][0].putToken(player);
		cells[2][1].putToken(player);
		cells[1][2].putToken(player);
		cells[0][3].putToken(player);
	}

	private void createLastColumnVerticalWin(Color player) {
		cells[0][6].putToken(player);
		cells[1][6].putToken(player);
		cells[2][6].putToken(player);
		cells[3][6].putToken(player);
	}

	//Dev feature. On veut pas print le board à chaque test donc ajouter dans le Then d'un test qui fail.
	private void printInConsole(Board board) {
		String output = "";
		for (int i = board.getNumberOfRows() - 1; i > -1; i--) {
			for (int j = 0; j < board.getNumberOfColumns(); j++) {
				if (cells[i][j].getColor().equals(Color.RED)) {
					output = output + "|" + ANSI_RED + "⚫" + ANSI_RESET + "|";
				} else if (cells[i][j].getColor().equals(Color.YELLOW)) {
					output = output + "|" + ANSI_YELLOW + "⚫" + ANSI_RESET + "|";
				} else {
					output = output + "|⚪|";
				}
			}
			output = output + "\n";
		}
		System.out.print(output);
		System.out.println("|                       |");
		System.out.println("|                       |");
		System.out.println();
		System.out.println();
	}
}